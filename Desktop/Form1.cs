﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MILESTONE
{
    public partial class Form1 : Form
    {
        //Class to organize the logic behind calculating the cost of an appointment.
        //The cost factor depends on which indexes from the "instructorComboBox" and
        //"opeingsComboBox which are chosen by the program user

        public class GymAppointment
        {
            private double charlieRate = 10.75;
            private double derrickRate = 11.50;
            private double jessicaRate = 15.00;
            private double koryRate = 18.00;
            int hours = 2;

            //I decided to only implement charlieCost from the GymAppointment class to suffice Milestone Two
            public double charlieCost
            {
                get => default(double);
                set
                {
                    double cost = charlieRate * hours;
                }
            }

            public double derrickCost
            {
                get => default(double);
                set
                {
                }
            }

            public double jessicaCost
            {
                get => default(double);
                set
                {
                }
            }

            public double koryCost
            {
                get => default(double);
                set
                {
                }
            }
        }

        public Form1()
        {
            InitializeComponent();

        }

        public void submitButton_Click(object sender, EventArgs e)
        {
            //Decided to add more to my form.
            if (string.IsNullOrEmpty(fNameTextBox.Text) || string.IsNullOrEmpty(lNameTextbox.Text)
                 || string.IsNullOrEmpty(phoneTextBox.Text) || string.IsNullOrEmpty(emailTextBox.Text))
            {
                MessageBox.Show("Please ensure all contact information is filled");
            }
            else
            {
                MessageBox.Show("Congratulations " + fNameTextBox.Text + ", your appointment has been scheduled!");
            }

            //Calling "charlieCost" from "GymAppointment" Class

            if (instructorComboBox.SelectedIndex == 0)
            {
                GymAppointment cost = new GymAppointment();
                MessageBox.Show("Your appointment expenses are " + cost.charlieCost);
            }


        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }


}
